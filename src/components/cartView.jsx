import React from 'react';
import ItemCart from './itemCart';
import styles from '../styles/cart.css';
import {observer} from "mobx-react/index";

@observer
export default class Main extends React.Component {
    render() {
        return (
            <div>
                <ul>
                    { this.props.cart.cart.map(
                        (item) => <ItemCart key={item.id} cart={this.props.cart} item={item} />
                    ) }
                </ul>
                <p className={styles.total}>TOTAL: $ { this.props.cart.total }</p>
            </div>
        );
    }
}

