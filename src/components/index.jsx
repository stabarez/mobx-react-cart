import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Main from './main';
import Cart from '../models/cart'

const cart = new Cart();

const App = () => (
    <MuiThemeProvider>
      <Main cart={cart}/>
    </MuiThemeProvider>
);

ReactDOM.render(
  <App />,
  document.getElementById('reactjs-app')
);
