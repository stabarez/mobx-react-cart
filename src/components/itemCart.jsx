import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const itemCart = (props) => {
  return (
      <div>
          <p>{props.item.name} X {props.item.quantity}</p>
          <p>$ {props.item.price}</p>
          <RaisedButton label="Remove" onClick={ () => { props.cart.removeFromCart(props.item.id)} } />
      </div>

  );
}

export default itemCart;