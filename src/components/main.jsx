import React from 'react';
import { observer } from 'mobx-react';
import {
    forEach,
} from 'lodash';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import RaisedButton from 'material-ui/RaisedButton';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import Selected from './selected';
import CartView from './cartView';
import styles from '../styles/main.css';
import { products } from '../mocks/mocks';

let productList = [];

@observer
export default class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      quantity: 1,
      viewCart: false
    };
  };

  componentWillMount() {
    forEach(products, (product) => {
      productList.push(<MenuItem value={product.id} key={product.id} primaryText={`${product.name}`} />);
    });
  }

  handleChangeSelected = (event, index, value) => this.setState({ selected: value });

  handleChangeQuantity = (event, index, value) => this.setState({ quantity: value });

  onNewItem = () => this.props.cart.addToCart({
    ...products[this.state.selected], 'quantity': this.state.quantity
  });

  onChangeCartView = () => this.setState({ viewCart: !this.state.viewCart });

  render() {
    // const { cart } = this.props;
    const itemSelected = this.state.selected !== null ? {...products[this.state.selected]} : null;

    return (
      <div className={styles.content}>
        <div className={styles.products}>
            <SelectField
                floatingLabelText="Products"
                value={this.state.selected}
                onChange={this.handleChangeSelected}
                maxHeight={200}
            >
                {productList}
            </SelectField>
            { itemSelected ?
                <Selected
                selected={itemSelected}
                quantity={this.state.quantity}
                addToCart={this.onNewItem}
                handleChangeQuantity={this.handleChangeQuantity}
                /> : null
            }
        </div>
        <div className={styles.cart}>
            <RaisedButton
              label={`View items on cart (${this.props.cart.cart.length})`}
              value={this.state.viewCart}
              onClick={this.onChangeCartView}
            />
            { this.state.viewCart ? <CartView cart={this.props.cart} /> : null }
        </div>
      </div>
    );
  }
}
