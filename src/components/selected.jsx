import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import styles from '../styles/selected.css';

const selected = (props) => {
    return (
        <div className={styles.selected}>
            <div className={styles.item}>
                <img src={props.selected.img}></img>
                <div className={styles.itemDescription}>
                    <h3>{props.selected.name}</h3>
                    <p>{props.selected.description}</p>
                    <p>Price: $ {props.selected.price}</p>
                </div>
            </div>
            <RaisedButton fullWidth={false} label="Add to Cart" onClick={props.addToCart}/>
            <SelectField
                floatingLabelText="Quantity"
                className={styles.quantitySelector}
                value={props.quantity}
                onChange={props.handleChangeQuantity}
                maxHeight={50}
            >
                <MenuItem value={1} key={1} primaryText="1" />
                <MenuItem value={2} key={2} primaryText="2" />
                <MenuItem value={3} key={3} primaryText="3" />
                <MenuItem value={4} key={4} primaryText="4" />
                <MenuItem value={5} key={5} primaryText="5" />
            </SelectField>
        </div>
    );
};

export default selected;