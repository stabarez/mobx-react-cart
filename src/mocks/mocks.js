const products = [
    {
        "id": 0,
        "name": "Beer",
        "price": 85,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed eiusmod tempor incidunt ut labore et dolore magna aliqua.",
        "img": "/src/img/beer.png"
    },
    {
        "id": 1,
        "name": "Gourmet Burger",
        "price": 250,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed eiusmod tempor incidunt ut labore et dolore magna aliqua.",
        "img": "/src/img/burger.png"
    },
    {
        "id": 2,
        "name": "French Fries",
        "price": 180,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed eiusmod tempor incidunt ut labore et dolore magna aliqua.",
        "img": "/src/img/fries.png"
    }
];

module.exports = {
    products,
};