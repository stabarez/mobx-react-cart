import { observable } from 'mobx';
import {
    forEach, filter
} from 'lodash';

export default class Cart {
	@observable cart = [];
    @observable total = 0;

	addToCart(product) {
		let existOnCart = false;
		forEach(this.cart, (productOnCart) => {
			if(productOnCart.id === product.id){
				existOnCart = true;
				productOnCart.quantity += product.quantity
			}
		});
		if(!existOnCart) {
			this.cart.push({
				id: product.id,
				name: product.name,
				price: product.price,
				quantity: product.quantity
			});
		}
		this.total += product.price * product.quantity
	}

	removeFromCart(id) {
		let productToDelete;

		this.cart = filter(this.cart, (product) => {
			if(product.id !== id) return product;
			productToDelete = product
		});

		if (productToDelete) {
            this.total -= productToDelete.price * productToDelete.quantity
        }
	}
}
